ARG PY_VERSION="latest"
ARG DEB_VERSION=""


FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX:+/}bitnami/python:${PY_VERSION}${DEB_VERSION:+-debian-}${DEB_VERSION}

RUN apt-get update && apt-get install -y nodejs npm libkrb5-dev && rm -rf /var/lib/apt/lists/*

RUN pip3 install --no-cache-dir uwsgi~=2.0 perun.proxygui[postgresql,kerberos]~=3.2 aup_manager[perun]~=2.0

COPY --chown=1001:1001 uwsgi.ini /etc/

EXPOSE 3031

CMD uwsgi /etc/uwsgi.ini
