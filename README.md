# docker-satosa-perun

![maintenance status: unmaintained](https://img.shields.io/maintenance/end%20of%20life/2023)

This image was replaced by [docker-perun-proxygui](https://gitlab.ics.muni.cz/perun-proxy-aai/containers/docker-perun-proxygui) and [docker-aup-manager](https://gitlab.ics.muni.cz/perun-proxy-aai/containers/docker-aup-manager).

## Original description

Docker image with apps made by Perun team to be used with the [SATOSA proxy](https://github.com/IdentityPython/SATOSA).

## Included apps

- [perun-proxygui](https://gitlab.ics.muni.cz/perun-proxy-aai/python/perun-proxygui)
- [aup_manager](https://gitlab.ics.muni.cz/perun-proxy-aai/python/aup-manager)
